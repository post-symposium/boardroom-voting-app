import { Component } from '@angular/core'
import { Router } from '@angular/router';

@Component({
  selector: 'app-comp',
  templateUrl: 'app.component.html'
})

export class AppComponent{
  title = 'app';
  constructor(private router: Router) {

  }

  notLogin() {
    return this.router.url != '/Login';
  }
}
