import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'

import { RouterModule, Routes } from '@angular/router';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav/navbar.component';
import { masterCardComponent } from './masterCard/card.component';
import { loginComponent } from './login/login.component';
import { addPollComponent } from './pages/addPoll/addPoll.component';
import { addUserComponent } from './pages/addUser/addUser.component';
import { viewPollComponent } from './pages/viewPoll/viewPoll.component';
import { viewUserComponent } from './pages/viewUser/viewUser.component';
import { homeComponent } from './pages/Home/home.component';
import { errorComponent } from './pages/errors/error.component';
import { environment } from '../environments/environment';
import { AuthGuard } from './auth/auth.guard';
import { successComponent } from './pages/success/success.component';
import { NgChartjsModule } from 'ng-chartjs';

const appRoutes: Routes = [
  {path: 'Home', component: masterCardComponent, canActivate: [AuthGuard]},
  {path: 'addPoll', component: masterCardComponent, canActivate: [AuthGuard]},
  {path: 'addUser', component: masterCardComponent, canActivate: [AuthGuard]},
  {path: 'viewPoll', component: masterCardComponent, canActivate: [AuthGuard]},
  {path: 'viewUser', component: masterCardComponent, canActivate: [AuthGuard]},
  {path: 'Success', component: masterCardComponent, canActivate: [AuthGuard]},
  {path: 'Login', component: loginComponent},
  {path: '', redirectTo:'/Login', pathMatch: 'full'},
  {path: '**', component:masterCardComponent, canActivate: [AuthGuard]}
]

@NgModule({
  declarations: [
    AppComponent,
    masterCardComponent,
    NavBarComponent,
    homeComponent,
    addPollComponent,
    addUserComponent,
    viewPollComponent,
    viewUserComponent,
    loginComponent,
    errorComponent,
    successComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp( environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule,
    HttpClientModule,
    NgChartjsModule.registerPlugin([])
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
