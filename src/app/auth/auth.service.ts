import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { User } from '../class/user';
import { GetService } from '../service/get.service';
import { PostService } from '../service/post.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService{

  public eventAuthError = new BehaviorSubject<string>("");
  eventAuthError$ = this.eventAuthError.asObservable()
  public loginAuthError = new BehaviorSubject<string>("");
  loginAuthError$ = this.eventAuthError.asObservable()
  private getUID = new BehaviorSubject<string>("");
  getUID$ = this.getUID.asObservable()
  private getDisplay = new BehaviorSubject<string>("");
  getDisplay$ = this.getDisplay.asObservable()
  newUser: any;
  userModel = new User('','','',0);
  displayName:string;
  private _isSignedIn: Boolean;
  
  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router,
    private _get: GetService,
    private _postService: PostService) {
      this.afAuth.authState.subscribe((user: firebase.User) => {
        if(user){
          this._isSignedIn = true;
        }else{
          this._isSignedIn = false;
        }
      });
     }

    get isSignedIn(){
      return this._isSignedIn;
    }

  login(email: string, password: string){
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .catch(error =>{
        this.loginAuthError.next(error);
      })
      .then(userCredential => {
        if(userCredential){
          this.router.navigate(['/Home']);
        }
      })
  }

  sendUser(frm){
    this.userModel.forename = frm.forename;
    this.userModel.surname = frm.surname;
    this.userModel.email = frm.email;
    if(frm.isAdmin == true){
      this.userModel.isAdmin = 1;
    }else{
      this.userModel.isAdmin = 0;
    }
    this._postService.sendUser(this.userModel)
        .subscribe(
            data => console.log('Success!', data),
            error => console.error('Error!', error)
        )
}
  createUser(user){
    this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password)
    .then(async userCredential =>{
      this.newUser = user;
      this.eventAuthError.next(null);
      this.sendUser(user);
    })
    .catch(error =>{
      this.eventAuthError.next(error);
    })
  }
}
