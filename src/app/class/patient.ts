export class Patient {
    constructor(
        public NHSNum: number,
        public Forename: string,
        public Surname: string,
        public Gender: string,
        public Age: number
    ){}
}
