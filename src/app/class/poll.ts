export class Poll {
    constructor(
        public QuestionTitle: string,
        public Password: string,
        public PatientNHSNo: number,
        public CaseDetails: string,
        public Response1: string,
        public Response2: string,
        public Response3: string,
        public Response4: string,
    ){}
}