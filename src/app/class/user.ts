export class User {
    constructor(
        public forename: string,
        public surname: string,
        public email: string,
        public isAdmin: number
    ){}
}