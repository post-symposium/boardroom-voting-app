import { Component, OnInit } from "@angular/core";
import { AuthService } from "../auth/auth.service";
import { GetService } from "../service/get.service";

@Component({
    selector: 'login-modal',
    templateUrl: 'login.component.html',
    styleUrls:['../masterCard/card.component.less', 'login.component.less'],
})

export class loginComponent implements OnInit{
    show: boolean;
    authError: any;
    users;
    error:boolean;
    admin:boolean;

    constructor(private auth: AuthService,
                private _get: GetService) {
       this.show = false;
    }

    ngOnInit(){
        this.auth.loginAuthError$.subscribe(data =>{
            this.authError = data;
            console.log(data);
        });
    }

    login(frm){
        this._get.getUsersByID(frm.value.email).subscribe(data => {
            this.users = data;
            if(this.users.isAdmin == 1){
                this.auth.login(frm.value.email, frm.value.password);
            }
            if(this.users.isAdmin == 0){
                this.authError = "User isn't an admin";
            }
            console.log(this.users);
        },
        error =>{
            console.log(error);
            this.authError = "Email Incorrect!";
        });
    }

    clear(){
        this.authError = "";
    }
}