import { Component, OnInit, Input, Inject } from '@angular/core'
import { Router } from '@angular/router';
import { NavBarComponent } from '../nav/navbar.component';
import { environment } from '../../environments/environment';


@Component({
  selector: 'masterCard',
  templateUrl: 'card.component.html',
  styleUrls:['card.component.less']
})

export class masterCardComponent implements OnInit {
  title = 'app'
  env = environment;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  isHome() {
    return this.router.url == '/Home';
  }
  isAddUser() {
    return this.router.url == '/addUser';
  }
  isAddPoll() {
    return this.router.url == '/addPoll';
  }
  isViewUser() {
    return this.router.url == '/viewUser';
  }
  isViewPoll() {
    return this.router.url == '/viewPoll';
  }
  isSuccess() {
    return this.router.url == '/Success';
  }
  isError() {
    return this.router.url == '/Error';
  }
  wrongPath() {
    return this.router.url == '**';
  }
}
