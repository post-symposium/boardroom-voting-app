import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { environment } from "../../environments/environment";
import { viewPollComponent } from "../pages/viewPoll/viewPoll.component";

@Component({
    selector: 'nav-bar',
    templateUrl: 'navbar.component.html',
    styleUrls:['navbar.component.less']
})


export class NavBarComponent implements OnInit{

    addIcon: string;
    closeBar: string;
    hamburger:string
    env = environment;

    constructor(private afAuth: AngularFireAuth,
                private router: Router){
        this.addIcon = '/assets/images/icons/add-24px.svg'
        this.closeBar = '/assets/images/icons/arrow_back_ios-24px.svg'
        this.hamburger = '/assets/images/icons/iconfinder_menu-alt_134216.svg'
    }
    
    ngOnInit() {
    }
      
    logout(){
        this.router.navigate(['/Login']);
        return this.afAuth.auth.signOut();
    }

    closeNav(){
        return this.env.closed = true;
    }

    openNav(){
        return this.env.closed = false;
    }
}