import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Patient } from '../../class/patient';
import { Poll } from '../../class/poll';
import { PostService } from '../../service/post.service'
import { Router } from '@angular/router';
import { GetService } from '../../service/get.service';
import { RequiredValidator } from '@angular/forms';

@Component({
    selector: 'addPoll',
    templateUrl: 'addPoll.component.html',
    styleUrls:['addPoll.component.less']
})

export class addPollComponent implements OnInit{
    
    patientID;

    ngOnInit(){}

    patientModel = new Patient(0, '', '', '', 0);
    pollModel = new Poll('', '', 0, '', '', '', '','');
    step: number = 1;
    responseCount: number = 0;
    trash: string;
    search: string;
    reverse: string;
    exists:boolean;
    error:boolean;

    constructor(private _postService: PostService,
                private router: Router,
                private _get: GetService){
        this.trash = 'assets/images/icons/iconmonstr-trash-can-1.svg'
        this.search = '/assets/images/icons/search.svg'
        this.reverse = '/assets/images/icons/reverse.svg'
        this.exists = false;
        this.error = false;
    }

    nhsNumSearch(){
            this._get.getPatientByID(this.patientModel.NHSNum).subscribe((data) => {
                this.patientID = data;
                this.patientModel.NHSNum = this.patientID[0].PatientNHSNum;
                this.patientModel.Forename = this.patientID[0].PatientFName;
                this.patientModel.Surname = this.patientID[0].PatientSName;
                this.patientModel.Gender = this.patientID[0].PatientGender;
                this.patientModel.Age = this.patientID[0].PatientAge;
                this.exists = true;
        });
    }

    stopAdd(){
        if(this.responseCount == 2){
            return true;
        }else{
            return false;
        }
    }

    add() {
        this.responseCount++;
    }

    remove() {
        this.responseCount--;
    }

    getStep(){
        return this.step;
    }
    nextPressed(){
        return this.step = 2;
    }
    backPressed(){
        return this.step = 1;
    }
    createPoll(){
        if(this.error == false){
            if(this.exists == false){
                this._postService.createPatient(this.patientModel)
                .subscribe(
                    data => console.log('Success!', data),
                    error => console.error('Error!', error)
                )
            }
            this.pollModel.PatientNHSNo = this.patientModel.NHSNum;
            this._postService.createPoll(this.pollModel)
                .subscribe(
                    data => console.log('Success!', data),
                    error => console.error('Error!', error)
                )
        this.router.navigate(['/Success']);
        }
    }
    revSearch(){
        this.patientModel.NHSNum = 0;
        this.patientModel.Forename = '';
        this.patientModel.Surname = '';
        this.patientModel.Gender = '';
        this.patientModel.Age = 0;
        return this.exists = false;
    }

    validate(){
        this.error = false;
        if(this.patientModel.NHSNum == 0 || this.patientModel.NHSNum == null){
            this.error = true;
        }
        if(this.patientModel.Forename == ''){
            this.error = true;
        }
        if(this.patientModel.Surname == ''){
            this.error = true;
        }
        if(this.patientModel.Gender == ''){
            this.error = true;
        }
        if(this.patientModel.Age == 0 || this.patientModel.Age == null){
            this.error = true;
        }
        if(this.pollModel.QuestionTitle == ''){
            this.error = true;
        }
        if(this.pollModel.CaseDetails == ''){
            this.error = true;
        }
        if(this.pollModel.Response1 == ''){
            this.error = true;
        }
        if(this.pollModel.Response2 == ''){
            this.error = true;
        }
        if(this.error == false){
            this.createPoll();
        }
    }
}