import { Component, OnInit } from '@angular/core'
import { AuthService } from '../../auth/auth.service';
import { PostService } from '../../service/post.service';
import { Router } from '@angular/router';

@Component({
    selector: 'addUser',
    templateUrl: 'addUser.component.html',
    styleUrls:['addUser.component.less'],
})

export class addUserComponent implements OnInit{
    
    authError: any;
    error:boolean;
    errorMessage:string;
    firebaseOk:boolean;
    

    constructor(private auth: AuthService,
                private _postService: PostService,
                private router: Router){
                    this.error = false;
                    this.errorMessage = undefined;
                }

    ngOnInit(){
        this.auth.eventAuthError$.subscribe( data =>{
            this.authError = data;    
            console.log(this.authError);   
        })
    }

    createUser(frm){
        this.validate(frm);
        if(this.errorMessage == undefined){
            this.auth.createUser(frm.value);
        }
    }

    async validate(user){
        this.error = false;
        this.errorMessage = undefined;
        console.log(user);
        if(user.value.forename == undefined || ""){
            this.error = true;
        }
        if(user.value.surname == undefined || ""){
            this.error = true;
        }
        if(user.value.password == undefined || ""){
            this.error = true;
        }
        if(user.value.email == undefined || ""){
            this.error = true;
        }
        if(this.authError != null){
            this.firebaseOk = false;
        }
        if(this.authError == null){
            this.firebaseOk = true;
        }
        if(this.error == true){
            this.errorMessage = 'Missing Required Fields';
        }
    }

    clear(){
        this.error = false;
        this.errorMessage = undefined;
        this.auth.eventAuthError.next(null);
    }
}