import { Component } from '@angular/core'

@Component({
    selector: 'error',
    templateUrl: 'error.component.html',
    styleUrls:['error.component.less']
})

export class errorComponent{
    error: string;

    constructor(){
        this.error = 'assets/images/icons/error.svg'
    }
}