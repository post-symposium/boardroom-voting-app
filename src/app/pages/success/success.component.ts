import { Component } from '@angular/core'

@Component({
    selector: 'success',
    templateUrl: 'success.component.html',
    styleUrls:['success.component.less']
})

export class successComponent{

    success: string;

    constructor(){
        this.success = 'assets/images/icons/success.svg'
    }
}