import { Component, OnInit } from '@angular/core'
import { GetService } from '../../service/get.service';
import { PostService } from '../../service/post.service';

@Component({
    selector: 'viewPoll',
    templateUrl: 'viewPoll.component.html',
    styleUrls:['viewPoll.component.less']
})

export class viewPollComponent implements OnInit{

    polls;
    pollByID;
    patient;
    psw: string;
    chosenQuestion:number;
    chosenPatient:number;
    previouseQuestion: number;
    confirmPage:boolean;
    confirm:boolean;
    results:boolean;
    resultsPage:boolean;
    responseLabel;
    responses;
    live:boolean;
    response3:boolean;
    response4:boolean;
    
    lineChartData: Array<any> = [
        {
          label: ['Responses'],
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(75,192,192,0.4)',
          borderColor: 'rgba(75,192,192,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(75,192,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
        },
      ];
      lineChartLabels: Array<any>;
      lineChartOptions: any = {
        responsive: true
      };
       lineChartLegend = false;
      lineChartType = 'bar';
      inlinePlugin: any;
      textPlugin: any;

    constructor(private _get: GetService,
                private _postService: PostService){
        this.psw = '';
        this.chosenQuestion = 0;
        this.chosenPatient = 0;
        this.confirmPage = false;
        this.resultsPage = false;
        this.live = false;
    }

    ngOnInit(){
        this._get.getPoll().subscribe((data) => {
            this.polls = data;
        });
    }

    questPati(){
        if(this.chosenQuestion == 0 && this.chosenPatient == 0){
            return true;
        }else{
            return false;
        }
    }

    backPatient(){
        this.chosenPatient = 0;
    }

    retrieveQuestion(psw){
        let num = this.chosenQuestion - 1
        var psw = this.polls[num].Password;
        this.getPollByID(psw);
    }

    getPollByID(psw){
        this._get.getPollByID(this.chosenQuestion, psw).subscribe((data) => {
            console.log(data);
            this.pollByID = data;
            if(this.pollByID.R3VoteCount == null){
                this.pollByID.R3VoteCount = 0;
            }
            if(this.pollByID.R4VoteCount == null){
                this.pollByID.R4VoteCount = 0;
            }
            this.showResults(this.chosenQuestion);
            console.log(this.pollByID);
        });
    }

    getPatientByID(){
        this.patient = null;
        this._get.getPatientByID(this.chosenPatient).subscribe((data) => {
            this.patient = data;
            console.log(this.patient);
        });
    }

    voteCount(id){
        let vote = this.polls[id-1].R1VoteCount +
                   this.polls[id-1].R2VoteCount +
                   this.polls[id-1].R3VoteCount +
                   this.polls[id-1].R4VoteCount;
        return vote;
    }

    isClosed(id){
        if(this.polls[id-1].IsArchived == 1){
            return 'Closed';
        } else{
            return 'Open';
        }
    }

    showResults(id){
        if(this.polls[id-1].IsArchived == 1){
            return this.results = true;
        }else{
            return this.results = false;
        }
    }

    isConfirmPage(){
        return this.confirmPage = true;
    }
    notConfirmPage(){
        this.confirmPage = false;
        this.resultsPage = false;
    }

    closePoll(){
        this._postService.closePoll(this.chosenQuestion)
            .subscribe(
                data => console.log('Success!', data),
                error => console.error('Error!', error)
            )
        this.getResults();
    }

    getResults(){
        this._get.getPoll().subscribe((data) => {
            this.polls = data;
        });
        this.confirmPage = false;
        if(this.pollByID.Response3 != null && this.pollByID.Response4 != null){
            this.lineChartLabels = [this.pollByID.Response1,
                                    this.pollByID.Response2,
                                    this.pollByID.Response3,
                                    this.pollByID.Response4];
            this.lineChartData = [{
                                    data: [this.pollByID.R1VoteCount,
                                           this.pollByID.R2VoteCount,
                                           this.pollByID.R3VoteCount,
                                           this.pollByID.R4VoteCount]
                                        }];
        }
        if(this.pollByID.Response3 != null && this.pollByID.Response4 == null){
            this.lineChartLabels = [this.pollByID.Response1,
                                    this.pollByID.Response2,
                                    this.pollByID.Response3
                                    ];
            this.lineChartData = [{
                                    data: [this.pollByID.R1VoteCount,
                                           this.pollByID.R2VoteCount,
                                           this.pollByID.R3VoteCount
                                          ]
                                        }];
        }
        if(this.pollByID.Response3 == null && this.pollByID.Response4 == null){
            this.lineChartLabels = [this.pollByID.Response1,
                                    this.pollByID.Response2
                                    ];
            this.lineChartData = [{
                                    data: [this.pollByID.R1VoteCount,
                                           this.pollByID.R2VoteCount
                                          ]
                                        }];
        }
        return this.resultsPage = true;
    }

    liveReponse(){
        this.live = true;
        this.isResponse3();
        this.isResponse4();
    }

    public backPressed(){
        this.live = false;
        this.pollByID = null;
        this.chosenPatient=0;
        this.chosenQuestion=0;
    }
    liveBack(){
        this.live = false;
    }
    isResponse3(){
        if(this.pollByID.Response3 != null && this.pollByID.Response3 != '' && this.pollByID.Response3 != undefined){
            this.response3 = true;
        }
        else{
            this.response3 = false;
        }
    }
    isResponse4(){
        if(this.pollByID.Response4 != null && this.pollByID.Response4 != '' && this.pollByID.Response4 != undefined){
            this.response4 = true;
        }
        else{
            this.response4 = false;
        }
    }
}