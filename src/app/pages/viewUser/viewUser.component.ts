import { Component, OnInit } from '@angular/core'
import { GetService } from '../../service/get.service';

@Component({
    selector: 'viewUser',
    templateUrl: 'viewUser.component.html',
    styleUrls:['viewUser.component.less']
})

export class viewUserComponent implements OnInit{

    users;
    admin:string;

    constructor(private _get: GetService){}

    ngOnInit(){
        this._get.getUsers().subscribe((data) => {
            this.users = data;
            console.log(this.users);
        });
    }

    isAdmin(id){
        console.log(id);
        if(this.users[id].isAdmin == 1){
            return this.admin = 'Yes';
        } else{
            return this.admin = 'No';
        }
    }
}