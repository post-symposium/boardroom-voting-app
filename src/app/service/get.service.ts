import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostService } from './post.service';

@Injectable({
  providedIn: 'root'
})
export class GetService {
  constructor(private _http: HttpClient, private serv: PostService) { 
  }

  public getUsers(){
    return this._http.get(this.serv._url + '/users');
  }

  public getUsersByID(uid){
    return this._http.get(this.serv._url + '/users/' + uid);
  }

  public getAdmin(){
    return this._http.get(this.serv._url + '/auth/');
  }

  public getPoll(){
    return this._http.get(this.serv._url + '/question');
  }

  public getPollByID(id, psw){
    return this._http.get(this.serv._url + '/question/' + id + '?password=' + psw);
  }
  
  public getPatientByID(id){
    return this._http.get(this.serv._url + '/patient/' + id);
  }

  public getPatient(){
    return this._http.get(this.serv._url + '/patient');
  }
}
