import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Poll } from '../class/poll';
import { Patient } from '../class/patient';
import { User } from '../class/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PostService {
//'http://localhost:3000'
  _url = 'http://94.174.192.92:3000';
  constructor(private _http: HttpClient, private router: Router) { }

  createPoll(poll: Poll){
    return this._http.post<any>(this._url + "/question", poll);
  }

  createPatient(patient: Patient){
    return this._http.post<any>((this._url + "/patient"), patient);
  }
  
  sendUser(user: User){
    this.router.navigate(['/Success']);
    return this._http.post<any>((this._url + "/users/createuser"), user);
  }

  closePoll(id){
    return this._http.post<any>((this._url + "/question/close/" + id),'');
  }
}
