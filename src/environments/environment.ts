// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  closed: false,
  production: false,
  firebase: {
    apiKey: "AIzaSyCxoWkbdNUcks6YwEZTiXmI4iHP1xkOAOA",
    authDomain: "post-symposium.firebaseapp.com",
    databaseURL: "https://post-symposium.firebaseio.com",
    projectId: "post-symposium",
    storageBucket: "post-symposium.appspot.com",
    messagingSenderId: "497667434343",
    appId: "1:497667434343:web:6da5027c534640bef471c2",
    measurementId: "G-VQWFNJ62SP"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
